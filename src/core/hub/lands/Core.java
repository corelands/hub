package core.hub.lands;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import core.hub.lands.api.NPCPlayer;
import core.hub.lands.api.pets.PetFollow;
import core.hub.lands.commands.CmdFly;
import core.hub.lands.commands.CmdNpc;
import core.hub.lands.commands.CmdSetLocation;
import core.hub.lands.events.*;
import core.hub.lands.events.items.hotbar.ItemCollectibles;
import core.hub.lands.events.items.hotbar.ItemMinigames;
import core.hub.lands.events.items.hotbar.ItemVisibility;
import core.hub.lands.managers.effects.EffectsAnimator;
import core.hub.lands.managers.scoreboard.ScoreboardAnimator;
import core.hub.lands.utils.PlayerUtils;
import core.hub.lands.utils.collectibles.effects.EffectsInventory;
import core.hub.lands.utils.collectibles.pets.PetsInventory;
import core.hub.lands.utils.items.gadgets.MagicStick;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class Core extends JavaPlugin {

    private static Core instance;
    private ProtocolManager manager;

    private ArrayList<Player> times;
    public ArrayList<NPCPlayer> npcs;

    public void onEnable() {
        times = new ArrayList<>();
        npcs = new ArrayList<>();
        instance = this;
        manager = ProtocolLibrary.getProtocolManager();

        registerEvents();
        registerCommands();

        new ScoreboardAnimator().runTaskTimer(this, 0, 20*60);
        new EffectsAnimator().runTaskTimer(this, 0, 2);
        new PetFollow().runTaskTimer(this, 0 , 1);

        for(Player all : Bukkit.getOnlinePlayers()) {
            new PlayerUtils(all).setup();
        }

        loadNpcs();

        registerPackets();

    }

    public void onDisable() {
        for(Player all : Bukkit.getOnlinePlayers()) {
            new PlayerUtils(all).disconnect();
        }
    }

    private void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EventPlayerQuit(), this);
        pm.registerEvents(new EventPlayerJoin(), this);
        pm.registerEvents(new EventPlayerDeath(), this);
        pm.registerEvents(new EventMainProtection(), this);
        pm.registerEvents(new EventPlayerChat(), this);

        pm.registerEvents(new ItemVisibility(), this);
        pm.registerEvents(new ItemMinigames(), this);
        pm.registerEvents(new ItemCollectibles(), this);

        pm.registerEvents(new EffectsInventory(), this);
        pm.registerEvents(new PetsInventory(), this);

        pm.registerEvents(new MagicStick(), this);
    }

    private void registerCommands() {
        getCommand("setlocation").setExecutor(new CmdSetLocation());
        getCommand("fly").setExecutor(new CmdFly());
        getCommand("npc").setExecutor(new CmdNpc());
    }

    private void registerPackets() {
        manager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
            @Override
            public void onPacketReceiving(PacketEvent event) {

                Player player = event.getPlayer();

                if(!times.contains(player)) {
                    if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                        int id = event.getPacket().getIntegers().read(0);
                        if(getConfig().getString("npcs.n"+id+".command") != null) {
                            player.sendMessage("§e[NPC] §7Trying to join the game...");
                        }
                    }
                    times.add(player);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            times.remove(player);
                        }
                    }.runTaskLater(Core.getInstance(), 20);
                }
            }
        });
    }

    private void loadNpcs() {
        if(getConfig().getConfigurationSection("npcs") != null) {
            for (String id : getConfig().getConfigurationSection("npcs").getKeys(false)) {
                String name = getConfig().getString("npcs." + id + ".name");

                Location loc = new Location(Bukkit.getWorld(getConfig().getString("npcs." + id + ".location.world")),
                        getConfig().getDouble("npcs." + id + ".location.x"),
                        getConfig().getDouble("npcs." + id + ".location.y"),
                        getConfig().getDouble("npcs." + id + ".location.z"),
                        (float) getConfig().getDouble("npcs." + id + ".location.yaw"),
                        (float) getConfig().getDouble("npcs." + id + ".location.pitch"));
                NPCPlayer npcPlayer = new NPCPlayer(loc, name, Integer.parseInt(id.replaceAll("n", "")));
                npcPlayer.spawn();
                npcs.add(npcPlayer);
            }
        }
    }

    public static Core getInstance() {
        return instance;
    }

}
