package core.hub.lands.api;

import core.hub.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;

public class LocationTeleport {

    private Location location;

    public LocationTeleport(String location) {
        FileConfiguration config = Core.getInstance().getConfig();
        String world = config.getString("locations." + location.toLowerCase() + ".world");
        double x = config.getDouble("locations." + location.toLowerCase() + ".x");
        double y = config.getDouble("locations." + location.toLowerCase() + ".y");
        double z = config.getDouble("locations." + location.toLowerCase() + ".z");
        float yaw = config.getLong("locations." + location.toLowerCase() + ".yaw");
        float pitch = config.getLong("locations." + location.toLowerCase() + ".pitch");
        this.location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    public void teleport(Entity e) {
        e.teleport(location);
    }

}
