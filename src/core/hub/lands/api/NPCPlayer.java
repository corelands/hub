package core.hub.lands.api;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import core.hub.lands.Core;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.UUID;

public class NPCPlayer {

    private Location location;
    private String name;
    private GameProfile gameProfile;
    private EntityPlayer entityPlayer;
    private int id;

    public NPCPlayer(Location location, String name, int id) {
        this.location = location;
        this.name = name;
        this.id = id;
    }

    public void create(String command) {
        Bukkit.broadcastMessage("§e[NPC] §7Created ID §a" + id);

        Core.getInstance().getConfig().set("npcs.n" + id + ".location.world", location.getWorld().getName());
        Core.getInstance().getConfig().set("npcs.n" + id + ".location.x", location.getX());
        Core.getInstance().getConfig().set("npcs.n" + id + ".location.y", location.getY());
        Core.getInstance().getConfig().set("npcs.n" + id + ".location.z", location.getZ());
        Core.getInstance().getConfig().set("npcs.n" + id + ".location.yaw", location.getYaw());
        Core.getInstance().getConfig().set("npcs.n" + id + ".location.pitch", location.getPitch());
        Core.getInstance().getConfig().set("npcs.n" + id + ".name", name);
        Core.getInstance().getConfig().set("npcs.n" + id + ".command", command);
        Core.getInstance().getConfig().set("npcs.n" + id + ".uuid", "95000eb4-de9f-4ee2-85f2-22ecd0ea04bc");
        Core.getInstance().getConfig().set("npcs.n" + id + ".texture_value", "eyJ0aW1lc3RhbXAiOjE1NjI1OTkxMjQ3OTksInByb2ZpbGVJZCI6IjkxZjA0ZmU5MGYzNjQzYjU4ZjIwZTMzNzVmODZkMzllIiwicHJvZmlsZU5hbWUiOiJTdG9ybVN0b3JteSIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTU0NWE0MTJhOWRlMzkxYzFjNDM4YmM1YzBlYjk5NGMxYTMzZTU4Yjc5YzM5MmEyNjJhMjc2YjkwYmNjZWI1MiJ9fX0=");
        Core.getInstance().getConfig().set("npcs.n" + id + ".texture_signature", "ELhETUqXBgyVZu2lWPW1tkZjzf7eTU8S80lAXn+oB6N22JAai4F081Md/3rdegNZAs7Lv4y59blZzxrCh/AK6eHM9r0VdeHTbRu0ILer3V55p8R7RoJSKZFWtmutqhhnPlVGOCWgZmrhmLR94vyx+VSSUy8ziza+WUhiDCxo8VX6l+lBanWH3Bw42ISiuV0ykfgVDxSeNPzaDbOh2tHD5YhtgUwU6+38x5keDQW6khv5QczbmLKIEcIq4RYZj5wDsvhhuwxdfFJWCly+syIBGWt5chY1DSli4SejARUdCVBhxXnNXP7zZrSgZD8AG60w9e2NRn/UKi+Z+qwQEi8/3BOuEZLz29Wi2UmyMUbKu+sawhxoPAJSfbaKlrlgHDhY35X2kTa+qAS6wMn0PxCfPiJQTH6tKSp1eDRoyhgttmzIrHWiB/JfHQ5fSQeccQ7Kp3Y9q4ql1Oh4/J1r2pSvGth/obNWKsrahESwnxBW04SeIiH+Rcuz4OfVQgE8sHXCOwYHhpMB6lNKXrdlGdnkDNeNGNel3lgXTSOhcj5Mmca31q3QlOcDrtLQNKVFBp7MQlFBdyGN/S046F7paFZZWJTUhBHXKO4hJV8r10TuculTslM9m5i/65qwaLFYyWLdMAeXie7kF6eoJl5GuJtcWkytmMihq3wQtvN+qLFiW4Y=");

        Core.getInstance().saveConfig();
        Core.getInstance().reloadConfig();
    }

    public void spawn() {
        MinecraftServer minecraftServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer worldServer = ((CraftWorld) location.getWorld()).getHandle();

        this.gameProfile = new GameProfile(UUID.fromString(Core.getInstance().getConfig().getString("npcs.n" + id + ".uuid")), Core.getInstance().getConfig().getString("npcs.n" + id + ".name"));
        this.gameProfile.getProperties().put("textures", new Property("textures", Core.getInstance().getConfig().getString("npcs.n" + id + ".texture_value"), Core.getInstance().getConfig().getString("npcs.n" + id + ".texture_signature")));

        this.entityPlayer = new EntityPlayer(minecraftServer, worldServer, gameProfile, new PlayerInteractManager(worldServer));
        this.entityPlayer.setPosition(location.getX(), location.getY(), location.getZ());
        this.entityPlayer.setHeadRotation((Math.round(location.getYaw()) + 270) % 360);

        this.entityPlayer.e(this.id);
    }

    public void show(Player player) {
        PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;

        playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
        playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(entityPlayer));

        entityPlayer.getDataWatcher().set(new DataWatcherObject<>(15, DataWatcherRegistry.a), (byte) 0xFF);

        PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();
        setValue(packet, "a", entityPlayer.getId());
        setValue(packet, "b", getFixLocation(location.getX()));
        setValue(packet, "c", getFixLocation(location.getY()));
        setValue(packet, "d", getFixLocation(location.getZ()));
        setValue(packet, "e", getFixRotation(location.getYaw()));
        setValue(packet, "f", getFixRotation(location.getPitch()));

        playerConnection.sendPacket(packet);
        PacketPlayOutEntity.PacketPlayOutEntityLook packet2 = new PacketPlayOutEntity.PacketPlayOutEntityLook(entityPlayer.getId(), getFixRotation(location.getYaw()),getFixRotation(location.getPitch()) , true);
        PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
        setValue(packetHead, "a", entityPlayer.getId());
        setValue(packetHead, "b", getFixRotation(location.getYaw()));


        playerConnection.sendPacket(packet2);
        playerConnection.sendPacket(packetHead);

        new BukkitRunnable() {
            public void run() {
                playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
            }
        }.runTaskLater(Core.getInstance(), 50);

    }

    public void setValue(Object obj,String name,Object value){
        try{
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        }catch(Exception e){}
    }

    public double getFixLocation(double pos){
        return pos;
    }

    public byte getFixRotation(float yawpitch){
        return (byte) ((int) (yawpitch * 256.0F / 360.0F));
    }

    public int getId() {
        return this.id;
    }

}
