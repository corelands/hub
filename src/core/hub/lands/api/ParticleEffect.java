package core.hub.lands.api;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

public enum ParticleEffect {

    HEART("HEART"),
    FIREWORKS_SPARK("FIREWORKS_SPARK"),
    FLAME("FLAME");

    private String name;

    ParticleEffect(String name) {
        this.name = name;
    }

    public void display(Player player, Location location, int quantity, double speed) {

        player.getWorld().spawnParticle(Particle.valueOf(name), location.getX(), location.getY(), location.getZ(), quantity, 0, 0, 0, speed);

    }

}
