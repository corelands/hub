package core.hub.lands.api.pets;

import net.minecraft.server.v1_14_R1.EntityInsentient;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

class Pet {

    private Player owner;
    private String name;
    private PetTypes type;
    private LivingEntity pet;

    Pet(Player owner, String name, PetTypes type) {
        this.owner = owner;
        this.name = name;
        this.type = type;
    }

    void spawn() {
        pet = (LivingEntity) owner.getWorld().spawn(owner.getLocation(), type.getClassType());
        pet.setCustomName(name);
        pet.setCustomNameVisible(true);
    }

    void walkTo(Location target, double speed) {
        EntityInsentient c = (EntityInsentient) ((CraftLivingEntity) pet).getHandle();
        c.getNavigation().a(target.getX(), target.getY(), target.getZ(), speed);
    }

    void remove() {
        pet.remove();
        owner = null;
        name = null;
        type = null;
        pet = null;
    }

    Player getOwner() {
        return owner;
    }

    void teleport(Location location) {
        pet.teleport(location);
    }

    Location getLocation() {
        return pet.getLocation();
    }

    PetTypes getType() {
        return type;
    }

}
