package core.hub.lands.api.pets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PetFollow extends BukkitRunnable {

    @Override
    public void run() {
        for (Pet pet : new PetManager().getPets()) {
            Player owner = pet.getOwner();
            if(pet.getLocation().getWorld() != owner.getWorld()) {
                new PetManager().respawnPet(owner);
            } else {
                double distance = pet.getLocation().distance(owner.getLocation());
                if (distance > 5) {
                    if (distance > 25 && owner.isOnGround()) {
                        pet.teleport(owner.getLocation());
                    }
                    pet.walkTo(owner.getLocation().clone().add(1, 0, 0), 1.7);
                }
            }
        }
    }

}
