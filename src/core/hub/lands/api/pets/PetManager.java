package core.hub.lands.api.pets;

import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;

public class PetManager {

    private static HashMap<Player, Pet> pets = new HashMap<>();

    public boolean hasPet(Player player, PetTypes type) {
        return pets.containsKey(player) && pets.get(player).getType().equals(type);
    }

    public void activePet(Player owner, PetTypes petType) {
        if(!pets.containsKey(owner)) {
            Pet pet = new Pet(owner, "§a" + owner.getName() + "'s Pet", petType);
            pets.put(owner, pet);
            pet.spawn();
        }
    }

    public void respawnPet(Player player) {
        PetTypes pet = pets.get(player).getType();
        remove(player);

        activePet(player, pet);
    }

    public void remove(Player player) {
        if(pets.containsKey(player)) {
            pets.get(player).remove();
            pets.remove(player);
        }
    }

    Collection<Pet> getPets() {
        return pets.values();
    }

}
