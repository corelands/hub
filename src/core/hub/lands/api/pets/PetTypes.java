package core.hub.lands.api.pets;

import org.bukkit.entity.*;

public enum PetTypes {

    DOG(Wolf.class),
    CAT(Cat.class),
    ZOMBIE(Zombie.class),
    PIG(Pig.class),
    COW(Cow.class),
    SKELETON(Skeleton.class),
    IRON_GOLEM(IronGolem.class);

    private Class<? extends Entity> classType;

    PetTypes(Class<? extends Entity> classType) {
        this.classType = classType;
    }

    public Class<? extends Entity> getClassType() {
        return classType;
    }

}
