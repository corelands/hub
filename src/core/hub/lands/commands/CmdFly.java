package core.hub.lands.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdFly implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;

        if(player.hasPermission("corelands.commands.fly")) {
            if(!player.getAllowFlight()) {
                player.setAllowFlight(true);
                player.sendMessage("§e[Hub] §7You can fly now.");
            } else {
                player.setAllowFlight(false);
                player.setFlying(false);
                player.sendMessage("§e[Hub] §7You can't fly anymore.");
            }
        }


        return false;
    }
}
