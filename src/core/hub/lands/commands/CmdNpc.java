package core.hub.lands.commands;

import core.hub.lands.api.Hologram;
import core.hub.lands.api.NPCPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdNpc implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.isOp()) {
            Player player = (Player) commandSender;

            if(args.length == 5) {
                NPCPlayer npcPlayer = new NPCPlayer(player.getLocation(), args[0].replaceAll("&", "§").replaceAll("-", " "), Integer.parseInt(args[3]));

                npcPlayer.create(args[4]);

                new Hologram(player.getLocation().clone().add(0, 1.4, 0), args[2].replaceAll("&", "§").replaceAll("-", " "));
                new Hologram(player.getLocation().clone().add(0, 1.1, 0), args[1].replaceAll("&", "§").replaceAll("-", " "));

                npcPlayer.spawn();

                for(Player p : Bukkit.getOnlinePlayers()) {
                    npcPlayer.show(p);
                }

                player.sendMessage("§e[NPC] §7Npc Spawned");
            }
        }
        return false;
    }
}
