package core.hub.lands.commands;

import core.hub.lands.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CmdSetLocation implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player p = (Player) commandSender;
        if(commandSender.hasPermission("corelands.command.setlocation")) {
            if(args.length == 1) {
                String location = args[0].toLowerCase();

                FileConfiguration config = Core.getInstance().getConfig();

                config.set("locations." + location + ".world", p.getLocation().getWorld().getName());
                config.set("locations." + location + ".x", p.getLocation().getX());
                config.set("locations." + location + ".y", p.getLocation().getY());
                config.set("locations." + location + ".z", p.getLocation().getZ());
                config.set("locations." + location + ".yaw", p.getLocation().getYaw());
                config.set("locations." + location + ".pitch", p.getLocation().getPitch());

                Core.getInstance().saveConfig();
                Core.getInstance().reloadConfig();

                commandSender.sendMessage("§aLocation §f" + location + "§a saved.");
            }
        }

        return false;
    }
}
