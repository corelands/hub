package core.hub.lands.events;

import core.hub.lands.Core;
import core.hub.lands.api.NPCPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class EventMainProtection implements Listener {

    @EventHandler
    public void noMobSpawn(CreatureSpawnEvent e) {
        if(!e.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.CUSTOM)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void noDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityTarget(EntityTargetLivingEntityEvent event){
        if (event.getTarget() instanceof LivingEntity){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void noLeavesDecay(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noEntityChange(EntityChangeBlockEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noRain(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noFire(EntityCombustEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noFood(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void noPickup(PlayerPickupItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent e) {
        for(NPCPlayer npcPlayer : Core.getInstance().npcs) {
            if(e.getPlayer().getWorld().getName().equals(Core.getInstance().getConfig().getString("npcs.n" + npcPlayer.getId() + ".location.world"))) {
                npcPlayer.show(e.getPlayer());
            }
        }
    }

}
