package core.hub.lands.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class EventPlayerChat implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        Player player = e.getPlayer();

        String message = (player.isOp()) ? e.getMessage().replaceAll("&", "§") : e.getMessage();

        Bukkit.broadcastMessage("§7[§b0§7] §8[Member] §f" + e.getPlayer().getName() + " §a► §7" + message);
    }

}
