package core.hub.lands.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class EventPlayerDeath implements Listener {

    @EventHandler
    public void onPlayerDie(PlayerDeathEvent e) {
        e.setDeathMessage(null);
    }

}
