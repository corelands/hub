package core.hub.lands.events;

import core.hub.lands.Core;
import core.hub.lands.api.NPCPlayer;
import core.hub.lands.managers.scoreboard.ScoreboardManager;
import core.hub.lands.utils.PlayerUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventPlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        PlayerUtils playerUtils = new PlayerUtils(e.getPlayer());
        playerUtils.setup();
        playerUtils.giveHotbar();
        new ScoreboardManager(e.getPlayer()).setScoreboard();

        for(NPCPlayer npcPlayer : Core.getInstance().npcs) {
            if(e.getPlayer().getWorld().getName().equals(Core.getInstance().getConfig().getString("npcs.n" + npcPlayer.getId() + ".location.world"))) {
                npcPlayer.show(e.getPlayer());
            }
        }

    }

}
