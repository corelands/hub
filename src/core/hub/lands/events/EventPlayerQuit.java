package core.hub.lands.events;

import core.hub.lands.utils.PlayerUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventPlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        PlayerUtils playerUtils = new PlayerUtils(e.getPlayer());
        playerUtils.disconnect();
        e.setQuitMessage(null);
    }
}
