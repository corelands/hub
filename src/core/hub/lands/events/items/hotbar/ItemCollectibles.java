package core.hub.lands.events.items.hotbar;

import core.hub.lands.managers.ActionListener;
import core.hub.lands.utils.ItemCreator;
import core.hub.lands.utils.collectibles.effects.EffectsInventory;
import core.hub.lands.utils.collectibles.pets.PetsInventory;
import core.hub.lands.utils.items.Hotbar;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class ItemCollectibles extends ActionListener {

    public ItemCollectibles() {
        name = "§eCollectibles";
        slots = 9 * 6;
    }

    public void setItems(Player p) {
        items.put(10, new ItemCreator(Material.BONE).setName("§fPets §e§lNEW").setLore(Arrays.asList(" ", " §aSpawn your little friend", " ")).getItem());
        items.put(13, new ItemCreator(Material.BLAZE_POWDER).setName("§eEffects §e§lNEW").setLore(Arrays.asList(" ", " §aUse beautiful effects", " ")).getItem());
        items.put(16, new ItemCreator(Material.BEACON).setName("§bHats §c§lSOON").setLore(Arrays.asList(" ", " §cComing soon...", " ")).getItem());
        items.put(37, new ItemCreator(Material.TNT).setName("§cGadgets §c§lSOON").setLore(Arrays.asList(" ", " §cComing soon...", " ")).getItem());
        items.put(40, new ItemCreator(Material.ZOMBIE_HEAD).setName("§aMorphs §c§lSOON").setLore(Arrays.asList(" ", " §cComing soon...", " ")).getItem());
        items.put(43, new ItemCreator(Material.LEATHER_CHESTPLATE).setName("§6Wardrobe §c§lSOON").setLore(Arrays.asList(" ", " §cComing soon...", " ")).getItem());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        ItemStack item = e.getItem();
        if(canOpen(e.getAction(), item, Hotbar.COLLECTIBLES.getItem())) {
            e.setCancelled(true);
            setItems(e.getPlayer());
            openInventory(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerUseItems(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            Player p = (Player) e.getWhoClicked();
            e.setCancelled(true);
            switch (e.getSlot()) {
                case 10:
                    new PetsInventory(p);
                    break;
                case 13:
                    new EffectsInventory(p);
                    break;
            }
        }
    }

}