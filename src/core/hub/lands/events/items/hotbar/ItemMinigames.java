package core.hub.lands.events.items.hotbar;

import core.hub.lands.api.LocationTeleport;
import core.hub.lands.managers.ActionListener;
import core.hub.lands.utils.ItemCreator;
import core.hub.lands.utils.items.Hotbar;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class ItemMinigames extends ActionListener {

    public ItemMinigames() {
        name = "§eMinigames";
        slots = 9 * 3;
    }

    public void setItems(Player p) {
        items.put(9, new ItemCreator(Material.BOOKSHELF).setName("§e§lNEW §eHUB").setLore(Arrays.asList(" ", " §aMain spawn", " ")).getItem());
        items.put(11, new ItemCreator(Material.BEACON).setName("§e§lNEW §dNebula").setLore(Arrays.asList(" ", " §aSpecial minigame", " ")).getItem());
        items.put(12, new ItemCreator(Material.GRASS_BLOCK).setName("§e§lNEW §aSkyWars").setLore(Arrays.asList(" ", " §aClassic minigame", " ")).getItem());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        ItemStack item = e.getItem();
        if(canOpen(e.getAction(), item, Hotbar.MINIGAMES.getItem())) {
            e.setCancelled(true);
            setItems(e.getPlayer());
            openInventory(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerUseItems(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            e.setCancelled(true);
            switch(e.getSlot()) {
                case 9:
                    new LocationTeleport("hub").teleport(e.getWhoClicked());
                    break;
                case 11:
                    new LocationTeleport("nebula").teleport(e.getWhoClicked());
                    break;
                case 12:
                    new LocationTeleport("skywars").teleport(e.getWhoClicked());
                    break;
            }
        }
    }

}
