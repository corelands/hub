package core.hub.lands.events.items.hotbar;

import core.hub.lands.managers.ActionListener;
import core.hub.lands.utils.ItemCreator;
import core.hub.lands.utils.items.Hotbar;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemVisibility extends ActionListener {

    public static ArrayList<Player> hiders = new ArrayList<>();

    public ItemVisibility() {
        name = "§aVisibility";
        slots = 9;
    }

    public void setItems(Player p) {

        if(hiders.contains(p)) {
            items.put(3, new ItemCreator(Material.ENDER_PEARL).setName("§aShow").setLore(Arrays.asList(" ", " §ashow all players", " ")).getItem());
            items.put(5, new ItemCreator(Material.ENDER_EYE).setName("§eHide").setLore(Arrays.asList(" ", " §ehide all players", " ")).addGlow().getItem());
        } else {
            items.put(3, new ItemCreator(Material.ENDER_PEARL).setName("§aShow").setLore(Arrays.asList(" ", " §ashow all players", " ")).addGlow().getItem());
            items.put(5, new ItemCreator(Material.ENDER_EYE).setName("§eHide").setLore(Arrays.asList(" ", " §ehide all players", " ")).getItem());
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        ItemStack item = e.getItem();
        if(canOpen(e.getAction(), item, Hotbar.VISIBILITY.getItem())) {
            e.setCancelled(true);
            setItems(e.getPlayer());
            openInventory(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerUseItems(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            Player p = (Player) e.getWhoClicked();
            e.setCancelled(true);
            switch (e.getSlot()) {
                case 3:
                    hiders.remove(p);
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        p.showPlayer(all);
                    }
                    p.closeInventory();
                    break;
                case 5:
                    if(!hiders.contains(p))
                        hiders.add(p);
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        if(all != p)
                            p.hidePlayer(all);
                    }
                    p.closeInventory();
                    break;
            }
        }
    }

}
