package core.hub.lands.managers;

import core.hub.lands.managers.faces.ItemFace;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class ActionListener implements Listener, ItemFace {

    public String name;
    protected int slots;

    protected boolean canOpen(Action action, ItemStack item, ItemStack otherItem) {
        return (action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_AIR)) && item != null && item.equals(otherItem);
    }

    public Inventory getInventory(Player p) {
        items.clear();
        setItems(p);
        Inventory inventory = Bukkit.createInventory(p, slots, name);
        for(int i = 0; i < slots; i++) {
            if(items.get(i)!=null){inventory.setItem(i, items.get(i));}
        }
        return inventory;
    }

    protected void openInventory(Player p) {
        items.clear();
        setItems(p);
        Inventory inventory = Bukkit.createInventory(p, slots, name);
        for(int i = 0; i < slots; i++) {
            if(items.get(i)!=null){inventory.setItem(i, items.get(i));}
        }
        p.openInventory(inventory);
    }

    @EventHandler
    public void antiMove(InventoryClickEvent e) {
        if(!e.getWhoClicked().isOp())
            e.setCancelled(true);
    }

    @EventHandler
    public void antiDrop(PlayerDropItemEvent e) {
        if(!e.getPlayer().isOp())
            e.setCancelled(true);
    }

    @EventHandler
    public void antiPickup(PlayerPickupItemEvent e) {
        if(!e.getPlayer().isOp())
            e.setCancelled(true);
    }

}
