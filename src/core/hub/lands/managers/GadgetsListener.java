package core.hub.lands.managers;

import core.hub.lands.managers.faces.GadgetsFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public abstract class GadgetsListener implements Listener, GadgetsFace {

    protected ItemStack gadget = null;

    @EventHandler
    public void onUseGadget(PlayerInteractEvent e) {
        setGadget();
        ItemStack item = e.getItem();
        if(canOpen(e.getAction(), item, gadget)) {
            e.setCancelled(true);
            toDo(e);
        }
    }

    private boolean canOpen(Action action, ItemStack item, ItemStack otherItem) {
        return (action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_AIR)) && item != null && item.equals(otherItem);
    }

}
