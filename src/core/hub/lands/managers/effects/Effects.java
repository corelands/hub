package core.hub.lands.managers.effects;

import org.bukkit.entity.Player;

import java.util.ArrayList;

public enum Effects {

    GLOBAL, LOVE, SPIRAL, FIRE_DEMON;

    private ArrayList<Player> players;

    Effects() {
        this.players = new ArrayList<>();
    }

    public void add(Player player) {
        this.players.add(player);
    }

    public void remove(Player player) {
        this.players.remove(player);
    }

    public boolean contains(Player player) {
        return this.players.contains(player);
    }

    public void removeAll(Player player) {
        for(Effects effect : Effects.values()) {
            effect.remove(player);
        }
    }

    public ArrayList<Player> getPlayers() {
        return this.players;
    }

}
