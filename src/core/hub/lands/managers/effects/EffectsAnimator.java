package core.hub.lands.managers.effects;

import core.hub.lands.api.ParticleEffect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class EffectsAnimator extends BukkitRunnable {

    @Override
    public void run() {
        loveEffect();
        fireDemon();
        spiral();
    }

    /*
    @Variables
     */
    double loveRadius = 1;
    double loveAngle = 0;
    /*
    @Effect
     */
    private void loveEffect() {


        double x = Math.cos(loveAngle) * loveRadius;
        double y = 2;
        double z = Math.sin(loveAngle) * loveRadius;

        if(loveAngle == Math.PI*2) {
            loveAngle = 0;
        }

        for(Player players : Effects.LOVE.getPlayers()) {
            Location loc = players.getLocation().clone().add(x, y, z);
            ParticleEffect.HEART.display(players, loc, 1, 0);
        }

        loveAngle+=Math.PI/6;

    }

    /*
    @Variables
     */
    double spiralAngle = 0;
    double spiralRadius = 1;
    double spiralHeight = 0;
    /*
        @Effect
         */
    public void spiral() {
        spiralAngle+=0.2;
        spiralHeight+=0.05;

        double x = Math.cos(spiralAngle) * spiralRadius;
        double y = spiralHeight;
        double z = Math.sin(spiralAngle) * spiralRadius;

        for(Player players : Effects.SPIRAL.getPlayers()) {
            Location loc = players.getLocation().clone().add(x, y, z);
            ParticleEffect.FIREWORKS_SPARK.display(players, loc, 1, 0);
        }

        if(spiralHeight > 2) {
            loveAngle = 0;
            spiralHeight = 0;
        }
    }

    /*
    @Variables
     */
    double testAngle = 0;
    double testRadius = 0;
    double testHeight = -1;
    /*
            @Effect
             */
    public void fireDemon() {
        testHeight += 0.1;
        if(testHeight < 1) {
            testRadius += 0.05;
        } else {
            testRadius -= 0.05;
        }

        double x = Math.cos(testAngle) * testRadius;
        double y = testHeight;
        double z = Math.sin(testAngle) * testRadius;

        for(Player players : Effects.FIRE_DEMON.getPlayers()) {
            ParticleEffect.FLAME.display(players, players.getLocation().clone().add(x, y, z), 1,0);
            ParticleEffect.FLAME.display(players, players.getLocation().clone().add(-x, y, z), 1,0);
            ParticleEffect.FLAME.display(players, players.getLocation().clone().add(-x, y, -z), 1,0);
            ParticleEffect.FLAME.display(players, players.getLocation().clone().add(x, y, -z), 1,0);
        }

        testAngle+=0.1;
        if(testHeight > 3) {
            testHeight = -1;
            testAngle = 0;
            testRadius = 0;
        }
    }

}
