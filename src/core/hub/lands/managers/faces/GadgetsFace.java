package core.hub.lands.managers.faces;

import org.bukkit.event.player.PlayerInteractEvent;

public interface GadgetsFace {

    void setGadget();
    void toDo(PlayerInteractEvent e);

}