package core.hub.lands.managers.faces;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public interface ItemFace {

    HashMap<Integer, ItemStack> items = new HashMap<>();
    void setItems(Player p);

}
