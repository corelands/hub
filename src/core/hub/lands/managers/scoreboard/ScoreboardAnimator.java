package core.hub.lands.managers.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardAnimator extends BukkitRunnable {

    @Override
    public void run() {
        for (Player all : Bukkit.getOnlinePlayers()) {
            Scoreboard board = all.getScoreboard();

            board.getTeam("rank").setSuffix("Member");

        }
    }

}
