package core.hub.lands.managers.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class ScoreboardManager {

    private Scoreboard board;
    private Objective obj;
    private Player p;

    public ScoreboardManager(Player p) {
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.obj = board.registerNewObjective("Hub", "CoreLands");
        this.obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.obj.setDisplayName("§e§lCoreLands");
        this.p = p;
    }

    public void setScoreboard() {

        obj.getScore("§a ").setScore(10);

        Team sugar = board.registerNewTeam("sugar");
        sugar.addEntry(" §fTokens: ");
        sugar.setSuffix("§a0");
        obj.getScore(" §fTokens: ").setScore(9);

        Team coins = board.registerNewTeam("coins");
        coins.addEntry(" §fEXP: ");
        coins.setSuffix("§b0");
        obj.getScore(" §fEXP: ").setScore(8);

        Team exp = board.registerNewTeam("exp");
        exp.addEntry(" §fCoins: ");
        exp.setSuffix("§e0");
        obj.getScore(" §fCoins: ").setScore(7);

        obj.getScore("§b").setScore(6);

        Team rank = board.registerNewTeam("rank");
        rank.addEntry(" §fRank: ");
        rank.setSuffix("§7Member");
        obj.getScore(" §fRank: ").setScore(5);

        Team achievements = board.registerNewTeam("achievements");
        achievements.addEntry(" §fAchievements: ");
        achievements.setSuffix("§a0");
        obj.getScore(" §fAchievements: ").setScore(4);

        obj.getScore("§c").setScore(3);

        Team crates = board.registerNewTeam("crates");
        crates.addEntry(" §fCrates: ");
        crates.setSuffix("§d0");
        obj.getScore(" §fCrates: ").setScore(2);

        obj.getScore("§d").setScore(1);
        obj.getScore("§emc.corelands.net").setScore(0);

        this.p.setScoreboard(board);
    }


}
