package core.hub.lands.utils;

import core.hub.lands.api.pets.PetManager;
import core.hub.lands.events.items.hotbar.ItemVisibility;
import core.hub.lands.managers.effects.Effects;
import core.hub.lands.utils.items.Hotbar;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;


public class PlayerUtils {

    private Player p;

    public PlayerUtils(Player p) {
        this.p = p;
    }

    public void setup() {
        p.setHealth(20);
        p.setFoodLevel(20);
        for(Player hiders : ItemVisibility.hiders) {
            hiders.hidePlayer(p);
        }
    }

    public void giveHotbar() {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setGameMode(GameMode.ADVENTURE);
        for(Hotbar hotbar : Hotbar.values()) {
            if(hotbar.equals(Hotbar.PROFILE))
                p.getInventory().setItem(hotbar.getSlot(), hotbar.getHead(p));
            else
                p.getInventory().setItem(hotbar.getSlot(), hotbar.getItem());
        }
    }

    public void disconnect() {
        new PetManager().remove(p);
        Effects.GLOBAL.removeAll(p);
    }

}
