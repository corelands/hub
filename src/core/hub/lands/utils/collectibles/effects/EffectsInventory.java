package core.hub.lands.utils.collectibles.effects;

import core.hub.lands.events.items.hotbar.ItemCollectibles;
import core.hub.lands.managers.effects.Effects;
import core.hub.lands.utils.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class EffectsInventory implements Listener {

    public EffectsInventory(){}

    private String name = "§eEffects";
    private Inventory inventory;

    private void setItems(Player player) {
        this.inventory = Bukkit.createInventory(player, 9 * 3, this.name);

        inventory.setItem(4, new ItemCreator(Material.RED_STAINED_GLASS_PANE).setName("§cRemove").setLore(Arrays.asList("", " §eRemove your current particles", "")).getItem());

        if(Effects.LOVE.contains(player))
            inventory.setItem(10, new ItemCreator(Material.LEGACY_RED_ROSE).setName("§cLove").setLore(Arrays.asList("", " §aNormal", "")).addGlow().getItem());
        else
            inventory.setItem(10, new ItemCreator(Material.LEGACY_RED_ROSE).setName("§cLove").setLore(Arrays.asList("", " §aNormal", "")).getItem());

        if(Effects.SPIRAL.contains(player))
            inventory.setItem(11, new ItemCreator(Material.FIREWORK_ROCKET).setName("§bSpiral").setLore(Arrays.asList("", " §dRare", "")).addGlow().getItem());
        else
            inventory.setItem(11, new ItemCreator(Material.FIREWORK_ROCKET).setName("§bSpiral").setLore(Arrays.asList("", " §dRare", "")).getItem());

        if(Effects.FIRE_DEMON.contains(player))
            inventory.setItem(12, new ItemCreator(Material.BLAZE_POWDER).setName("§6Fire Demon").setLore(Arrays.asList("", " §5Epic", "")).addGlow().getItem());
        else
            inventory.setItem(12, new ItemCreator(Material.BLAZE_POWDER).setName("§6Fire Demon").setLore(Arrays.asList("", " §5Epic", "")).getItem());

        inventory.setItem(13, new ItemCreator(Material.RED_DYE).setName("§cSoon").setLore(Arrays.asList("", " §cSoon", "")).getItem());
        inventory.setItem(14, new ItemCreator(Material.RED_DYE).setName("§cSoon").setLore(Arrays.asList("", " §cSoon", "")).getItem());
        inventory.setItem(15, new ItemCreator(Material.RED_DYE).setName("§cSoon").setLore(Arrays.asList("", " §cSoon", "")).getItem());
        inventory.setItem(16, new ItemCreator(Material.RED_DYE).setName("§cSoon").setLore(Arrays.asList("", " §cSoon", "")).getItem());

        inventory.setItem(22, new ItemCreator(Material.CHEST).setName("§eCollectibles").setLore(Arrays.asList("", " §eGet back to the collectibles", "")).getItem());
    }

    public EffectsInventory(Player player) {
        this.setItems(player);
        player.openInventory(this.inventory);
    }

    @EventHandler
    public void onPlayerChooseEffect(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            Player p = (Player) e.getWhoClicked();
            e.setCancelled(true);
            switch (e.getSlot()) {
                case 4:
                    Effects.GLOBAL.removeAll(p);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 10:
                    Effects.GLOBAL.removeAll(p);
                    Effects.LOVE.add(p);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 11:
                    Effects.GLOBAL.removeAll(p);
                    Effects.SPIRAL.add(p);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 12:
                    Effects.GLOBAL.removeAll(p);
                    Effects.FIRE_DEMON.add(p);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 22:
                    p.openInventory(new ItemCollectibles().getInventory(p));
                    break;
            }
        }
    }

}
