package core.hub.lands.utils.collectibles.pets;

import core.hub.lands.api.pets.PetManager;
import core.hub.lands.api.pets.PetTypes;
import core.hub.lands.events.items.hotbar.ItemCollectibles;
import core.hub.lands.utils.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class PetsInventory implements Listener {

    public PetsInventory(){}

    private String name = "§fPets";
    private Inventory inventory;

    private void setItems(Player player) {
        this.inventory = Bukkit.createInventory(player, 9 * 3, this.name);

        inventory.setItem(4, new ItemCreator(Material.RED_STAINED_GLASS_PANE).setName("§cRemove").setLore(Arrays.asList("", " §eRemove your current pet", "")).getItem());

        if(new PetManager().hasPet(player, PetTypes.DOG))
            inventory.setItem(10, new ItemCreator(Material.BONE).setName("§7Dog").addGlow().getItem());
        else
            inventory.setItem(10, new ItemCreator(Material.BONE).setName("§7Dog").getItem());

        if(new PetManager().hasPet(player, PetTypes.CAT))
            inventory.setItem(11, new ItemCreator(Material.LEGACY_RAW_FISH).setName("§eCat").addGlow().getItem());
        else
            inventory.setItem(11, new ItemCreator(Material.LEGACY_RAW_FISH).setName("§eCat").getItem());

        if(new PetManager().hasPet(player, PetTypes.COW))
            inventory.setItem(12, new ItemCreator(Material.LEATHER).setName("§6Cow").addGlow().getItem());
        else
            inventory.setItem(12, new ItemCreator(Material.LEATHER).setName("§6Cow").getItem());

        if(new PetManager().hasPet(player, PetTypes.PIG))
            inventory.setItem(13, new ItemCreator(Material.CARROT).setName("§dPig").addGlow().getItem());
        else
            inventory.setItem(13, new ItemCreator(Material.CARROT).setName("§dPig").getItem());

        if(new PetManager().hasPet(player, PetTypes.SKELETON))
            inventory.setItem(14, new ItemCreator(Material.ARROW).setName("§fSkeleton").addGlow().getItem());
        else
            inventory.setItem(14, new ItemCreator(Material.ARROW).setName("§fSkeleton").getItem());

        if(new PetManager().hasPet(player, PetTypes.ZOMBIE))
            inventory.setItem(15, new ItemCreator(Material.ROTTEN_FLESH).setName("§aZombie").addGlow().getItem());
        else
            inventory.setItem(15, new ItemCreator(Material.ROTTEN_FLESH).setName("§aZombie").getItem());

        if(new PetManager().hasPet(player, PetTypes.IRON_GOLEM))
            inventory.setItem(16, new ItemCreator(Material.IRON_INGOT).setName("§7Iron Gollem").addGlow().getItem());
        else
            inventory.setItem(16, new ItemCreator(Material.IRON_INGOT).setName("§7Iron Gollem").getItem());

        inventory.setItem(22, new ItemCreator(Material.CHEST).setName("§eCollectibles").setLore(Arrays.asList("", " §eGet back to the collectibles", "")).getItem());
    }

    public PetsInventory(Player player) {
        this.setItems(player);
        player.openInventory(this.inventory);
    }

    @EventHandler
    public void onPlayerChooseEffect(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            Player p = (Player) e.getWhoClicked();
            e.setCancelled(true);
            switch (e.getSlot()) {
                case 4:
                    new PetManager().remove(p);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 10:
                    new PetManager().activePet(p, PetTypes.DOG);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 11:
                    new PetManager().activePet(p, PetTypes.CAT);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 12:
                    new PetManager().activePet(p, PetTypes.COW);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 13:
                    new PetManager().activePet(p, PetTypes.PIG);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 14:
                    new PetManager().activePet(p, PetTypes.SKELETON);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 15:
                    new PetManager().activePet(p, PetTypes.ZOMBIE);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 16:
                    new PetManager().activePet(p, PetTypes.IRON_GOLEM);
                    setItems(p);
                    p.openInventory(this.inventory);
                    p.updateInventory();
                    break;
                case 22:
                    p.openInventory(new ItemCollectibles().getInventory(p));
                    break;
            }
        }
    }

}
