package core.hub.lands.utils.items;

import core.hub.lands.utils.ItemCreator;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public enum Gadgets {

    MAGIC_STICK(new ItemCreator(Material.BLAZE_ROD).setName("§eMagic Stick §7(Right-Click)").setLore(Arrays.asList(
            " ",
            " §cShoot some magic fireballs",
            " ")).getItem());

    private ItemStack item;

    Gadgets(ItemStack item) {
        this.item = item;
    }

    public ItemStack getItem() {
        return this.item;
    }

}
