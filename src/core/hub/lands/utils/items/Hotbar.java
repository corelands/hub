package core.hub.lands.utils.items;

import core.hub.lands.utils.ItemCreator;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public enum Hotbar {

    MINIGAMES(new ItemCreator(Material.NETHER_STAR).setName("§eMinigames §7(Right-Click)").setLore(Arrays.asList(
            " ",
            " §aChoose a minigame to play",
            " ")).getItem(), 4),
    COLLECTIBLES(new ItemCreator(Material.CHEST).setName("§eCollectibles §7(Right-Click)").setLore(Arrays.asList(
            " ",
            " §aA lot of cool stuffs",
            " ")).getItem(), 8),
    PROFILE(new ItemCreator(Material.PLAYER_HEAD).setName("§eProfile §7(Right-Click)").setLore(Arrays.asList(
            " ",
            " §aSee your status",
            " ")).getItem(), 0),
    GADGET(Gadgets.MAGIC_STICK.getItem(), 1),
    VISIBILITY(new ItemCreator(Material.GRAY_DYE).setName("§eVisibility §7(Right-Click)").setLore(Arrays.asList(
            " ",
            " §aSee your status",
            " ")).getItem(), 7);


    private ItemStack item;
    private int slot;

    Hotbar(ItemStack item, int slot) {
        this.item = item;
        this.slot = slot;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public int getSlot() {
        return slot;
    }

    public ItemStack getHead(Player p) {
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setOwner(p.getName());
        item.setItemMeta(meta);
        return item;
    }

}
