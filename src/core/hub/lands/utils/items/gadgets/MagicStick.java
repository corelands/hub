package core.hub.lands.utils.items.gadgets;

import core.hub.lands.managers.GadgetsListener;
import core.hub.lands.managers.faces.GadgetsFace;
import core.hub.lands.utils.items.Gadgets;
import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;

public class MagicStick extends GadgetsListener implements GadgetsFace {

    private ArrayList<Projectile> projectiles = new ArrayList<>();

    public void setGadget() {
        this.gadget = Gadgets.MAGIC_STICK.getItem();
    }

    public void toDo(PlayerInteractEvent e) {
        e.setCancelled(true);
        Player p = e.getPlayer();
        p.launchProjectile(Snowball.class);
        EnderPearl ball2 = p.launchProjectile(EnderPearl.class);
        projectiles.add(ball2);
    }

    @EventHandler
    public void onHit(ProjectileHitEvent e) {
        if(projectiles.contains(e.getEntity())) {
            e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.ENTITY_CAT_PURREOW, 1f, 1f);
            projectiles.remove(e.getEntity());
        }
    }

    @EventHandler
    public void noTeleport(PlayerTeleportEvent e) {
        if(e.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
            e.setCancelled(true);
        }
    }

}
